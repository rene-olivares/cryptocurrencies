<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$app = new \Slim\App;

$app->get('/api/{amounts}/{currencies}/{cryptos}', function (Request $request, Response $response, array $args) {
	// Tomamos los parametros necesarios y validamos espacios
    $amounts_arr = explode(",", str_replace(" ", "", $args['amounts']));
    $currencies_arr = explode(",", str_replace(" ", "", $args['currencies']));
    $cryptos_arr = explode(",", str_replace(" ", "", $args['cryptos']));
    
    // Formamos el texto para la API de CryptoCompare
    $currencies_str = implode(",", $currencies_arr);

    // Recorrido de Cantidades/Montos
    $cryptos_curr = array();
    foreach ($amounts_arr as $key => $amount) {
        // Tiene que ser un valor numerico
        if(is_numeric($amount)){    
            // Obtenemos de la API de CryptoCompare los cambios para cada Tipo
            foreach ($cryptos_arr as $key2 => $crypto) {    
                $cryptos_curr[$crypto] = array();
                $json = file_get_contents('https://min-api.cryptocompare.com/data/price?fsym='.$crypto.'&tsyms='.$currencies_str);
                $json_aux = json_decode($json,true);
                // Si retorna 'Response', hay algo mal e ignoramos 
                if(isset($json_aux['Response'])){
                    if($json_aux['Response'] == "Error"){
                        $cryptos_curr[$crypto] = array();
                    }
                }else{
                    $cryptos_curr[$crypto] = $json_aux;
                }
                foreach ($cryptos_curr[$crypto] as $currency => $val_currency) {
                    echo number_format($amount)." ".$currency." = ".(($amount/$val_currency))." ".$crypto."<br>";
                }
            }
            echo "<br>";
        }
        
    }
});

$app->run();

?>